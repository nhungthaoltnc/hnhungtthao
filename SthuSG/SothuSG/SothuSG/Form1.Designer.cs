﻿namespace SothuSG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labeltime = new System.Windows.Forms.Label();
            this.butluu = new System.Windows.Forms.Button();
            this.thumoi = new System.Windows.Forms.Label();
            this.dsthu = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.Controls.Add(this.labeltime, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.butluu, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.thumoi, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dsthu, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.listBox2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.listBox1, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(32, 27);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(355, 251);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // labeltime
            // 
            this.labeltime.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.labeltime, 2);
            this.labeltime.Location = new System.Drawing.Point(3, 205);
            this.labeltime.Name = "labeltime";
            this.labeltime.Size = new System.Drawing.Size(174, 13);
            this.labeltime.TabIndex = 19;
            this.labeltime.Text = "bây giờ, ngày 25 tháng 3 năm 2015";
            // 
            // butluu
            // 
            this.butluu.Location = new System.Drawing.Point(264, 208);
            this.butluu.Name = "butluu";
            this.butluu.Size = new System.Drawing.Size(88, 25);
            this.butluu.TabIndex = 18;
            this.butluu.Text = "Lưu danh sách";
            this.toolTip1.SetToolTip(this.butluu, "Luu ds thu vao file ds thu.txt");
            this.butluu.UseVisualStyleBackColor = true;
            this.butluu.Click += new System.EventHandler(this.save);
            // 
            // thumoi
            // 
            this.thumoi.AutoSize = true;
            this.thumoi.Location = new System.Drawing.Point(3, 0);
            this.thumoi.Name = "thumoi";
            this.thumoi.Size = new System.Drawing.Size(45, 13);
            this.thumoi.TabIndex = 3;
            this.thumoi.Text = "Thu moi";
            // 
            // dsthu
            // 
            this.dsthu.AutoSize = true;
            this.dsthu.Location = new System.Drawing.Point(133, 0);
            this.dsthu.Name = "dsthu";
            this.dsthu.Size = new System.Drawing.Size(77, 13);
            this.dsthu.TabIndex = 10;
            this.dsthu.Text = "Danh sach thu";
            // 
            // listBox2
            // 
            this.listBox2.AllowDrop = true;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(133, 16);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(125, 186);
            this.listBox2.TabIndex = 3;
            this.listBox2.DragDrop += new System.Windows.Forms.DragEventHandler(this.ListDS_DragDrop);
            this.listBox2.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListBox_DragEnter);
            // 
            // listBox1
            // 
            this.listBox1.AllowDrop = true;
            this.listBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Khỉ",
            "Mèo",
            "Cọp",
            "Sư tử",
            "Beo",
            "Rắn hổ mang",
            "Vượn"});
            this.listBox1.Location = new System.Drawing.Point(3, 16);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox1.Size = new System.Drawing.Size(124, 186);
            this.listBox1.TabIndex = 2;
            this.listBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListBox_DragEnter);
            this.listBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListBox_MouseDown);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 309);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label thumoi;
        private System.Windows.Forms.Label dsthu;
        private System.Windows.Forms.Label labeltime;
        private System.Windows.Forms.Button butluu;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.ListBox listBox1;
    }
}

