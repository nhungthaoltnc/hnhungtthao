﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace calculator2
{
    public partial class Form1 : Form
    {
        Double ketqua = 0;
        String thaotac = "";
        bool thaotachd = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void butnhapso_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0")||(thaotachd))
                textBox1.Clear();
            thaotachd = false;
            Button button = (Button)sender;
            textBox1.Text = textBox1.Text + button.Text;
            
        }

        private void butthapphan_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Contains("."))
            {
                return;
            }
            textBox1.Text += butthapphan.Text;
        }

        private void tinhtoan_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (ketqua != 0)
            {
                button18.PerformClick();
                thaotachd = true;
            }
            else

            {
                thaotac = button.Text;
                ketqua = Double.Parse(textBox1.Text);
                thaotachd = true;
            }
        }

        private void butCE(object sender, EventArgs e)
        {
            textBox1.Text = "0";
        }

        private void butC(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            ketqua = 0;
        }

        private void butBang(object sender, EventArgs e)
        {
            switch(thaotac)
            {
                case"+":
                    textBox1.Text = (ketqua + Double.Parse(textBox1.Text)).ToString(); break;

                case "-":
                    textBox1.Text = (ketqua - Double.Parse(textBox1.Text)).ToString(); break;

                case "x":
                    textBox1.Text = (ketqua * Double.Parse(textBox1.Text)).ToString(); break;

                case ":":
                    textBox1.Text = (ketqua / Double.Parse(textBox1.Text)).ToString(); break;

                default:
                    break;
            }
        }

        private void butSoam_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Contains("-"))
            {
                textBox1.Text = textBox1.Text.Remove(0, 1);
            }
            else
                textBox1.Text = "-" + textBox1.Text;
        }

        private void butSqrt(object sender, EventArgs e)
        {
            textBox1.Text = Math.Sqrt(Double.Parse(textBox1.Text)).ToString();
        }
    }
}
