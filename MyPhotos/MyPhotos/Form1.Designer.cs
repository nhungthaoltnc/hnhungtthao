﻿namespace MyPhotos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pbxPhoto = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnufile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnunew = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuopen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.mnusave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnusaveas = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuprint = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuprintpre = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuexit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuedit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnucut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnucopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnupaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuadd = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuremove = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuView = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMenuphoto = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuScale = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStretch = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuActual = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sttInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.sttImageSize = new System.Windows.Forms.ToolStripStatusLabel();
            this.sttAlbumPos = new System.Windows.Forms.ToolStripStatusLabel();
            this.nextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPhoto)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.ctxMenuphoto.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbxPhoto
            // 
            this.pbxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxPhoto.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.pbxPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbxPhoto.Location = new System.Drawing.Point(44, 51);
            this.pbxPhoto.Name = "pbxPhoto";
            this.pbxPhoto.Size = new System.Drawing.Size(196, 159);
            this.pbxPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxPhoto.TabIndex = 1;
            this.pbxPhoto.TabStop = false;
            this.pbxPhoto.Click += new System.EventHandler(this.pbxPhoto_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnufile,
            this.mnuedit,
            this.mnuView,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 2;
            // 
            // mnufile
            // 
            this.mnufile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnunew,
            this.mnuopen,
            this.toolStripSeparator,
            this.mnusave,
            this.mnusaveas,
            this.toolStripSeparator2,
            this.mnuprint,
            this.mnuprintpre,
            this.toolStripSeparator3,
            this.mnuexit});
            this.mnufile.Name = "mnufile";
            this.mnufile.Size = new System.Drawing.Size(37, 20);
            this.mnufile.Text = "&File";
            // 
            // mnunew
            // 
            this.mnunew.Image = ((System.Drawing.Image)(resources.GetObject("mnunew.Image")));
            this.mnunew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnunew.Name = "mnunew";
            this.mnunew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.mnunew.Size = new System.Drawing.Size(152, 22);
            this.mnunew.Text = "&New";
            this.mnunew.Click += new System.EventHandler(this.mnunew_Click);
            // 
            // mnuopen
            // 
            this.mnuopen.Image = ((System.Drawing.Image)(resources.GetObject("mnuopen.Image")));
            this.mnuopen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuopen.Name = "mnuopen";
            this.mnuopen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.mnuopen.Size = new System.Drawing.Size(152, 22);
            this.mnuopen.Text = "&Open";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(149, 6);
            // 
            // mnusave
            // 
            this.mnusave.Image = ((System.Drawing.Image)(resources.GetObject("mnusave.Image")));
            this.mnusave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnusave.Name = "mnusave";
            this.mnusave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnusave.Size = new System.Drawing.Size(152, 22);
            this.mnusave.Text = "&Save";
            this.mnusave.Click += new System.EventHandler(this.mnusave_Click);
            // 
            // mnusaveas
            // 
            this.mnusaveas.Name = "mnusaveas";
            this.mnusaveas.Size = new System.Drawing.Size(152, 22);
            this.mnusaveas.Text = "Save &As";
            this.mnusaveas.Click += new System.EventHandler(this.mnusaveas_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // mnuprint
            // 
            this.mnuprint.Enabled = false;
            this.mnuprint.Image = ((System.Drawing.Image)(resources.GetObject("mnuprint.Image")));
            this.mnuprint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuprint.Name = "mnuprint";
            this.mnuprint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.mnuprint.Size = new System.Drawing.Size(152, 22);
            this.mnuprint.Text = "&Print";
            this.mnuprint.Visible = false;
            // 
            // mnuprintpre
            // 
            this.mnuprintpre.Enabled = false;
            this.mnuprintpre.Image = ((System.Drawing.Image)(resources.GetObject("mnuprintpre.Image")));
            this.mnuprintpre.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuprintpre.Name = "mnuprintpre";
            this.mnuprintpre.Size = new System.Drawing.Size(152, 22);
            this.mnuprintpre.Text = "Print Pre&view";
            this.mnuprintpre.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
            // 
            // mnuexit
            // 
            this.mnuexit.Name = "mnuexit";
            this.mnuexit.Size = new System.Drawing.Size(152, 22);
            this.mnuexit.Text = "E&xit";
            this.mnuexit.Click += new System.EventHandler(this.mnuexit_Click);
            // 
            // mnuedit
            // 
            this.mnuedit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnucut,
            this.mnucopy,
            this.mnupaste,
            this.toolStripSeparator5,
            this.mnuadd,
            this.mnuremove});
            this.mnuedit.Name = "mnuedit";
            this.mnuedit.Size = new System.Drawing.Size(39, 20);
            this.mnuedit.Text = "&Edit";
            // 
            // mnucut
            // 
            this.mnucut.Image = ((System.Drawing.Image)(resources.GetObject("mnucut.Image")));
            this.mnucut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnucut.Name = "mnucut";
            this.mnucut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.mnucut.Size = new System.Drawing.Size(190, 22);
            this.mnucut.Text = "Cu&t";
            // 
            // mnucopy
            // 
            this.mnucopy.Image = ((System.Drawing.Image)(resources.GetObject("mnucopy.Image")));
            this.mnucopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnucopy.Name = "mnucopy";
            this.mnucopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.mnucopy.Size = new System.Drawing.Size(190, 22);
            this.mnucopy.Text = "&Copy";
            // 
            // mnupaste
            // 
            this.mnupaste.Image = ((System.Drawing.Image)(resources.GetObject("mnupaste.Image")));
            this.mnupaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnupaste.Name = "mnupaste";
            this.mnupaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.mnupaste.Size = new System.Drawing.Size(190, 22);
            this.mnupaste.Text = "&Paste";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(187, 6);
            // 
            // mnuadd
            // 
            this.mnuadd.Name = "mnuadd";
            this.mnuadd.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.A)));
            this.mnuadd.Size = new System.Drawing.Size(190, 22);
            this.mnuadd.Text = "Ad&d";
            this.mnuadd.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // mnuremove
            // 
            this.mnuremove.Name = "mnuremove";
            this.mnuremove.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.R)));
            this.mnuremove.Size = new System.Drawing.Size(190, 22);
            this.mnuremove.Text = "Remo&ve";
            this.mnuremove.Click += new System.EventHandler(this.mnuremove_Click);
            // 
            // mnuView
            // 
            this.mnuView.DropDown = this.ctxMenuphoto;
            this.mnuView.Name = "mnuView";
            this.mnuView.Size = new System.Drawing.Size(44, 20);
            this.mnuView.Text = "View";
            // 
            // ctxMenuphoto
            // 
            this.ctxMenuphoto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuImage});
            this.ctxMenuphoto.Name = "ctxMenuphoto";
            this.ctxMenuphoto.Size = new System.Drawing.Size(108, 26);
            // 
            // mnuImage
            // 
            this.mnuImage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuScale,
            this.mnuStretch,
            this.mnuActual});
            this.mnuImage.Name = "mnuImage";
            this.mnuImage.Size = new System.Drawing.Size(107, 22);
            this.mnuImage.Text = "Image";
            this.mnuImage.DropDownOpening += new System.EventHandler(this.mnuImage_DropDownOpening);
            this.mnuImage.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ProccessPhoto);
            // 
            // mnuScale
            // 
            this.mnuScale.Checked = true;
            this.mnuScale.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnuScale.Name = "mnuScale";
            this.mnuScale.Size = new System.Drawing.Size(139, 22);
            this.mnuScale.Tag = "Zoom";
            this.mnuScale.Text = "&Scale to fit";
            // 
            // mnuStretch
            // 
            this.mnuStretch.Name = "mnuStretch";
            this.mnuStretch.Size = new System.Drawing.Size(139, 22);
            this.mnuStretch.Tag = "StretchImage";
            this.mnuStretch.Text = "S&tretch to fit";
            // 
            // mnuActual
            // 
            this.mnuActual.Name = "mnuActual";
            this.mnuActual.Size = new System.Drawing.Size(139, 22);
            this.mnuActual.Tag = "Normal";
            this.mnuActual.Text = "&Actual size";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sttInfo,
            this.sttImageSize,
            this.sttAlbumPos});
            this.statusStrip1.Location = new System.Drawing.Point(0, 237);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(284, 24);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sttInfo
            // 
            this.sttInfo.AccessibleName = "";
            this.sttInfo.AutoSize = false;
            this.sttInfo.Name = "sttInfo";
            this.sttInfo.Size = new System.Drawing.Size(209, 19);
            this.sttInfo.Spring = true;
            this.sttInfo.Text = "Desc";
            this.sttInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sttImageSize
            // 
            this.sttImageSize.AccessibleName = "";
            this.sttImageSize.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.sttImageSize.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.sttImageSize.Name = "sttImageSize";
            this.sttImageSize.Size = new System.Drawing.Size(36, 19);
            this.sttImageSize.Text = "WxH";
            // 
            // sttAlbumPos
            // 
            this.sttAlbumPos.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.sttAlbumPos.Name = "sttAlbumPos";
            this.sttAlbumPos.Size = new System.Drawing.Size(24, 19);
            this.sttAlbumPos.Text = "1/1";
            // 
            // nextToolStripMenuItem
            // 
            this.nextToolStripMenuItem.Name = "nextToolStripMenuItem";
            this.nextToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.N)));
            this.nextToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.nextToolStripMenuItem.Text = "&Next";
            // 
            // previousToolStripMenuItem
            // 
            this.previousToolStripMenuItem.Name = "previousToolStripMenuItem";
            this.previousToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.P)));
            this.previousToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.previousToolStripMenuItem.Text = "&Previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ContextMenuStrip = this.ctxMenuphoto;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pbxPhoto);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxPhoto)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ctxMenuphoto.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxPhoto;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ContextMenuStrip ctxMenuphoto;
        private System.Windows.Forms.ToolStripMenuItem mnuView;
        private System.Windows.Forms.ToolStripMenuItem mnuImage;
        private System.Windows.Forms.ToolStripMenuItem mnuScale;
        private System.Windows.Forms.ToolStripMenuItem mnuStretch;
        private System.Windows.Forms.ToolStripMenuItem mnuActual;
        private System.Windows.Forms.ToolStripStatusLabel sttInfo;
        private System.Windows.Forms.ToolStripStatusLabel sttImageSize;
        private System.Windows.Forms.ToolStripStatusLabel sttAlbumPos;
        private System.Windows.Forms.ToolStripMenuItem mnufile;
        private System.Windows.Forms.ToolStripMenuItem mnunew;
        private System.Windows.Forms.ToolStripMenuItem mnuopen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem mnusave;
        private System.Windows.Forms.ToolStripMenuItem mnusaveas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuprint;
        private System.Windows.Forms.ToolStripMenuItem mnuprintpre;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuexit;
        private System.Windows.Forms.ToolStripMenuItem mnuedit;
        private System.Windows.Forms.ToolStripMenuItem mnucut;
        private System.Windows.Forms.ToolStripMenuItem mnucopy;
        private System.Windows.Forms.ToolStripMenuItem mnupaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuadd;
        private System.Windows.Forms.ToolStripMenuItem mnuremove;
        private System.Windows.Forms.ToolStripMenuItem nextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}

