﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;

namespace PhotoAlbum
{
    class PhotoAlbum : Collection<Photograph>, IDisposable
    {
        public class AlbumStorageException : Exception
        {
            public AlbumStorageException() : base() { }
            public AlbumStorageException(string msg)
                : base(msg) { }
            public AlbumStorageException(string msg,
            Exception inner)
                : base(msg, inner) { }
        }
        public static class AlbumStorage
        {
            static private int CurrentVersion = 63;
        }
        static public void WriteAlbum(PhotoAlbum album, string path)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(path, false);
                sw.WriteLine(CurrentVersion.ToString());
                // Store each photo separately
                foreach (Photograph p in album)
                    WritePhoto(sw, p);
                // Reset changed after all photos written
                album.HasChanged = false;
            }
            catch (UnauthorizedAccessException uax)
            {
                throw new AlbumStorageException(
                "Unable to access album " + path, uax);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
        static private void WritePhoto(StreamWriter sw, Photograph p)
        {
            sw.WriteLine(p.FileName);
            sw.WriteLine(p.Caption != null
            ? p.Caption : "");
            sw.WriteLine(p.DateTaken.ToString());
            sw.WriteLine(p.Photographer != null
            ? p.Photographer : "");
            sw.WriteLine(p.Notes != null
            ? p.Notes : "");
        }
        static public PhotoAlbum ReadAlbum(string path)
        {
            StreamReader sr = null;
            try
            {
                sr = new StreamReader(path);
                string version = sr.ReadLine();
                PhotoAlbum album = new PhotoAlbum();
                switch (version)
                {
                    case "63":
                        ReadAlbumV63(sr, album);
                        break;
                    default:
                        throw new AlbumStorageException("Unrecognized album version")
                }
                album.HasChanged = false;
                return album;
            }
            catch (FileNotFoundException fnx)
            {
                throw new AlbumStorageException(
                    "Unable to read album " + path, fnx);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        static private void ReadAlbumV63(StreamReader sr, PhotoAlbum album)
        {
            // Read each photo into album.
            Photograph p;
            do
            {
                p = ReadPhotoV63(sr);
                if (p != null)
                    album.Add(p);
            } while (p != null);
        }
        static private Photograph ReadPhotoV63(StreamReader sr)
        {
            // Presume at the start of photo
            string file = sr.ReadLine();
            if (file == null || file.Length == 0)
                return null;
            // File not null, should find photo
            Photograph p = new Photograph(file);
            p.Caption = sr.ReadLine();
            p.DateTaken
            = DateTime.Parse(sr.ReadLine());
            p.Photographer = sr.ReadLine();
            p.Notes = sr.ReadLine();
            return p;
        }
        public void Dispose()
        {
            foreach (Photograph p in this)
                p.Dispose();
        }

        private bool hasChanged = false;
        public bool HasChanged
        {
            get 
            {
                if (hasChanged) return true;
                foreach (Photograph p in this)
                    if (p.HasChanged) return true;
                return false;
            }
            set
            {
                hasChanged = value;
                if (value == false)
                    foreach (Photograph p in this)
                        p.HasChanged = false;
            }
        }
             public Photograph Add(string filename)
             {
                 Photograph p = new Photograph(filename);
                 base.Add(p);
                 return p;
             }

             protected override void ClearItems()
             {
                 if(Count > 0)
                 {
                 Dispose();
                 base.ClearItems();
                 HasChanged = true;
                 }
             }

             protected override void InsertItem(int index, Photograph item)
             {
                 base.InsertItem(index, item);
                 HasChanged = true;
             }

             protected override void RemoveItem(int index)
             {
                 Items[index].Dispose();
                 base.RemoveItem(index);
                 HasChanged = true;
             }

             protected override void SetItem(int index, Photograph item)
             {
                 base.SetItem(index, item);
                 HasChanged = true;
             }

        }
    }
