﻿namespace calculator
{
    partial class frm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeltinh = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btncham = new System.Windows.Forms.Button();
            this.btnchia = new System.Windows.Forms.Button();
            this.btnnhan = new System.Windows.Forms.Button();
            this.btntru = new System.Windows.Forms.Button();
            this.btncong = new System.Windows.Forms.Button();
            this.btnbang = new System.Windows.Forms.Button();
            this.btnce = new System.Windows.Forms.Button();
            this.btncanbac = new System.Windows.Forms.Button();
            this.btnam = new System.Windows.Forms.Button();
            this.btnphantram = new System.Windows.Forms.Button();
            this.btnc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labeltinh
            // 
            this.labeltinh.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labeltinh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labeltinh.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeltinh.Location = new System.Drawing.Point(12, 9);
            this.labeltinh.Name = "labeltinh";
            this.labeltinh.Size = new System.Drawing.Size(304, 45);
            this.labeltinh.TabIndex = 0;
            this.labeltinh.Text = "0";
            this.labeltinh.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(22, 121);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(50, 50);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(78, 293);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(106, 50);
            this.btn0.TabIndex = 2;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(134, 237);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(50, 50);
            this.btn9.TabIndex = 3;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(78, 237);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(50, 50);
            this.btn8.TabIndex = 4;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(22, 237);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(50, 50);
            this.btn7.TabIndex = 5;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(134, 181);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(50, 50);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(78, 181);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(50, 50);
            this.btn5.TabIndex = 7;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(22, 181);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(50, 50);
            this.btn4.TabIndex = 8;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(134, 121);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(50, 50);
            this.btn3.TabIndex = 9;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(78, 121);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(50, 50);
            this.btn2.TabIndex = 10;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.nhapso_click);
            // 
            // btncham
            // 
            this.btncham.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncham.Location = new System.Drawing.Point(22, 293);
            this.btncham.Name = "btncham";
            this.btncham.Size = new System.Drawing.Size(50, 50);
            this.btncham.TabIndex = 11;
            this.btncham.Text = ".";
            this.btncham.UseVisualStyleBackColor = true;
            this.btncham.Click += new System.EventHandler(this.btncham_Click);
            // 
            // btnchia
            // 
            this.btnchia.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchia.Location = new System.Drawing.Point(190, 293);
            this.btnchia.Name = "btnchia";
            this.btnchia.Size = new System.Drawing.Size(50, 50);
            this.btnchia.TabIndex = 12;
            this.btnchia.Text = "/";
            this.btnchia.UseVisualStyleBackColor = true;
            this.btnchia.Click += new System.EventHandler(this.pheptoan_Click);
            // 
            // btnnhan
            // 
            this.btnnhan.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnhan.Location = new System.Drawing.Point(190, 237);
            this.btnnhan.Name = "btnnhan";
            this.btnnhan.Size = new System.Drawing.Size(50, 50);
            this.btnnhan.TabIndex = 13;
            this.btnnhan.Text = "*";
            this.btnnhan.UseVisualStyleBackColor = true;
            this.btnnhan.Click += new System.EventHandler(this.pheptoan_Click);
            // 
            // btntru
            // 
            this.btntru.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntru.Location = new System.Drawing.Point(190, 181);
            this.btntru.Name = "btntru";
            this.btntru.Size = new System.Drawing.Size(50, 50);
            this.btntru.TabIndex = 14;
            this.btntru.Text = "-";
            this.btntru.UseVisualStyleBackColor = true;
            this.btntru.Click += new System.EventHandler(this.pheptoan_Click);
            // 
            // btncong
            // 
            this.btncong.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncong.Location = new System.Drawing.Point(190, 121);
            this.btncong.Name = "btncong";
            this.btncong.Size = new System.Drawing.Size(50, 50);
            this.btncong.TabIndex = 15;
            this.btncong.Text = "+";
            this.btncong.UseVisualStyleBackColor = true;
            this.btncong.Click += new System.EventHandler(this.pheptoan_Click);
            // 
            // btnbang
            // 
            this.btnbang.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbang.Location = new System.Drawing.Point(246, 181);
            this.btnbang.Name = "btnbang";
            this.btnbang.Size = new System.Drawing.Size(50, 106);
            this.btnbang.TabIndex = 16;
            this.btnbang.Text = "=";
            this.btnbang.UseVisualStyleBackColor = true;
            this.btnbang.Click += new System.EventHandler(this.btnbang_Click);
            // 
            // btnce
            // 
            this.btnce.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnce.Location = new System.Drawing.Point(190, 66);
            this.btnce.Name = "btnce";
            this.btnce.Size = new System.Drawing.Size(106, 50);
            this.btnce.TabIndex = 17;
            this.btnce.Text = "CE";
            this.btnce.UseVisualStyleBackColor = true;
            this.btnce.Click += new System.EventHandler(this.btnce_Click);
            // 
            // btncanbac
            // 
            this.btncanbac.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncanbac.Location = new System.Drawing.Point(78, 67);
            this.btncanbac.Name = "btncanbac";
            this.btncanbac.Size = new System.Drawing.Size(50, 50);
            this.btncanbac.TabIndex = 18;
            this.btncanbac.Text = "sqrt";
            this.btncanbac.UseVisualStyleBackColor = true;
            this.btncanbac.Click += new System.EventHandler(this.btncanbac_Click);
            // 
            // btnam
            // 
            this.btnam.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnam.Location = new System.Drawing.Point(134, 66);
            this.btnam.Name = "btnam";
            this.btnam.Size = new System.Drawing.Size(50, 50);
            this.btnam.TabIndex = 19;
            this.btnam.Text = "+/-";
            this.btnam.UseVisualStyleBackColor = true;
            this.btnam.Click += new System.EventHandler(this.btnam_Click);
            // 
            // btnphantram
            // 
            this.btnphantram.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnphantram.Location = new System.Drawing.Point(22, 67);
            this.btnphantram.Name = "btnphantram";
            this.btnphantram.Size = new System.Drawing.Size(50, 50);
            this.btnphantram.TabIndex = 20;
            this.btnphantram.Text = "%";
            this.btnphantram.UseVisualStyleBackColor = true;
            this.btnphantram.Click += new System.EventHandler(this.pheptoan_Click);
            // 
            // btnc
            // 
            this.btnc.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnc.Location = new System.Drawing.Point(246, 122);
            this.btnc.Name = "btnc";
            this.btnc.Size = new System.Drawing.Size(50, 50);
            this.btnc.TabIndex = 21;
            this.btnc.Text = "C";
            this.btnc.UseVisualStyleBackColor = true;
            this.btnc.Click += new System.EventHandler(this.btnc_Click);
            // 
            // frm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 355);
            this.Controls.Add(this.btnc);
            this.Controls.Add(this.btnphantram);
            this.Controls.Add(this.btnam);
            this.Controls.Add(this.btncanbac);
            this.Controls.Add(this.btnce);
            this.Controls.Add(this.btnbang);
            this.Controls.Add(this.btncong);
            this.Controls.Add(this.btntru);
            this.Controls.Add(this.btnnhan);
            this.Controls.Add(this.btnchia);
            this.Controls.Add(this.btncham);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.labeltinh);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm1";
            this.Text = "Calculator";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.form1_keypress);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labeltinh;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btncham;
        private System.Windows.Forms.Button btnchia;
        private System.Windows.Forms.Button btnnhan;
        private System.Windows.Forms.Button btntru;
        private System.Windows.Forms.Button btncong;
        private System.Windows.Forms.Button btnbang;
        private System.Windows.Forms.Button btnce;
        private System.Windows.Forms.Button btncanbac;
        private System.Windows.Forms.Button btnam;
        private System.Windows.Forms.Button btnphantram;
        private System.Windows.Forms.Button btnc;

    }
}

