﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace calculator
{
    public partial class frm1 : Form
    {
        string pheptoan = "";
        string dau = "";
        double nho = 0;
        bool isTypingnumber = false;//dữ nguyên số phía trước
        public frm1()
        {
            InitializeComponent();//lời gọi hàm khởi tạo thành phần//
        }
        private void btnbang_Click(object sender, EventArgs e)
        {
            isTypingnumber = false;
            double ketqua = 0.0;
            switch (pheptoan)
            {
                case "+":
                    ketqua = nho + double.Parse(labeltinh.Text);
                    break;
                case "-":
                    ketqua = nho - double.Parse(labeltinh.Text);
                    break;
                case "*":
                    ketqua = nho * double.Parse(labeltinh.Text);
                    break;
                case "/":
                    ketqua = nho / double.Parse(labeltinh.Text);
                    break;
                case "%":
                    ketqua = nho / 100;
                    break;
            }
            labeltinh.Text = ketqua.ToString();
        }
        private void nhapso_click(object sender, EventArgs e)
        {
            if (isTypingnumber)
                labeltinh.Text = labeltinh.Text + ((Button)sender).Text;
            else
            {
                labeltinh.Text = ((Button)sender).Text;
                isTypingnumber = true;
            }
        }

        private void pheptoan_Click(object sender, EventArgs e)
        {
            if(nho != 0)
            {
                btnbang.PerformClick();
                isTypingnumber = true;
                pheptoan = ((Button)sender).Text;
            }
            isTypingnumber = false;
            Button b = (Button)sender;
            pheptoan = b.Text;
            nho = double.Parse(labeltinh.Text);
        }

        private void btnce_Click(object sender, EventArgs e)
        {
            labeltinh.Text = "0";
        }

        private void btncham_Click(object sender, EventArgs e)
        {
            if (!labeltinh.Text.Contains("."))
            {
                labeltinh.Text =labeltinh.Text + btncham.Text;
            }
        }

        private void form1_keypress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar.ToString())
            {
                case "0":
                    btn0.PerformClick();
                    break;
                case "1":
                    btn1.PerformClick();
                    break;
                case "2":
                    btn2.PerformClick();
                    break;
                case "3":
                    btn3.PerformClick();
                    break;
                case "4":
                    btn4.PerformClick();
                    break;
                case "5":
                    btn5.PerformClick();
                    break;
                case "6":
                    btn6.PerformClick();
                    break;
                case "7":
                    btn7.PerformClick();
                    break;
                case "8":
                    btn8.PerformClick();
                    break;
                case "9":
                    btn9.PerformClick();
                    break;
                case ".":
                    btncham.PerformClick();
                    break;
                case "+":
                    btncong.PerformClick();
                    break;
                case "-":
                    btntru.PerformClick();
                    break;
                case "*":
                    btnnhan.PerformClick();
                    break;
                case "/":
                    btnchia.PerformClick();
                    break;
                case "=":
                    btnbang.PerformClick();
                    break;
                default:
                    break;
            }
        }

        private void btncanbac_Click(object sender, EventArgs e)
        {
            Button b =(Button)sender;
            if (nho != 0)
            {
                if (b.Text == "sqrt")
                    labeltinh.Text = Math.Sqrt(double.Parse(labeltinh.Text)).ToString();
                isTypingnumber = true;
                pheptoan = b.Text;
            }
            else if (b.Text == "sqrt")
            {
                labeltinh.Text = Math.Sqrt(double.Parse(labeltinh.Text)).ToString();
                nho = Math.Sqrt(double.Parse(labeltinh.Text));
            }
            else
            {
                pheptoan = b.Text;
                nho = double.Parse(labeltinh.Text);
                isTypingnumber = true;
            }
        }

        private void btnc_Click(object sender, EventArgs e)
        {
            labeltinh.Text = "0";
            nho = 0;
        }
    }
}

