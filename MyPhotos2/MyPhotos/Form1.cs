﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PhotoAlbum;

namespace MyPhotos
{
    public partial class Form1 : Form
    {
        private AlbumManager manager;
        private AlbumManager Manager
        {
            get { return manager; }
            set { manager = value; }
        }
        public Form1()
        {
            InitializeComponent();
            NewAlbum();
            mnuView.DropDown = ctxMenuPhoto;
        }
        private void DisplayAlbum()
        {
            pbxPhoto.Image = Manager.CurrentImage;
            SetStatusStrip();
            SetTitleBar();
        }

        private void btnload_Click(object sender, EventArgs e) //load ảnh
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open Photo";
            dlg.Filter = "jpg file(*.jpg)|*.jpg|All file (*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pbxPhoto.Image = new Bitmap(dlg.OpenFile());
                    SetStatusStrip(dlg.FileName);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Unable to load files" + ex.Message);
                    pbxPhoto.Image = null;

                }
            }
            dlg.Dispose();
        }


        private void ProccessPhoto(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            string enumVal = item.Tag as string; //ep kieu xuat ra enum
            if (enumVal != null)
            {
                pbxPhoto.SizeMode = (PictureBoxSizeMode)Enum.Parse(typeof(PictureBoxSizeMode), enumVal);
            }
        }

        private void mnuImage_DropDownOpening(object sender, EventArgs e)
        {
            ToolStripDropDownItem parent = (ToolStripDropDownItem)sender;
            if (parent != null)
            {
                string enumVal = pbxPhoto.SizeMode.ToString(); //dua ra chuoi cac gtri cua thuoc tinh/doituong
                foreach (ToolStripMenuItem item in parent.DropDownItems)
                {
                    item.Enabled = (pbxPhoto.Image != null);
                    item.Checked = item.Tag.Equals(enumVal);
                }
            }
        }


        private void SetStatusStrip()
        {
            if (pbxPhoto.Image != null)
            {
                sttInfo.Text = Manager.Current.FileName;
                sttImageSize.Text = string.Format("{0:#}x{1:#}", pbxPhoto.Image.Width, pbxPhoto.Image.Height);
                sttAlbumPos.Text = String.Format(" {0:0}/{1:0} ",Manager.Index + 1,Manager.Album.Count);
            }
            else
            {
                sttInfo.Text = null;
                sttImageSize.Text = null;
                sttAlbumPos.Text = null;
            }
        }

        
        private void mnuexit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Add Photos";
            dlg.Multiselect = true;
            dlg.Filter
            = "Image Files (JPEG, GIF, BMP, etc.)|"
            + "*.jpg;*.jpeg;*.gif;*.bmp;"
            + "*.tif;*.tiff;*.png|"
            + "JPEG files (*.jpg;*.jpeg)|*.jpg;*.jpeg|"
            + "GIF files (*.gif)|*.gif|"
            + "BMP files (*.bmp)|*.bmp|"
            + "TIFF files (*.tif;*.tiff)|*.tif;*.tiff|"
            + "PNG files (*.png)|*.png|"
            + "All files (*.*)|*.*";
            dlg.InitialDirectory
            = Environment.CurrentDirectory;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string[] files = dlg.FileNames;
                int index = 0;
                foreach (string s in files)
                {
                    Photograph photo = new Photograph(s);
// Add the file (if not already present)
                    index = Manager.Album.IndexOf(photo);
                    if (index < 0)
                        Manager.Album.Add(photo);
                    else
                        photo.Dispose(); // photo already there
                    }
                Manager.Index = Manager.Album.Count - 1;  
            }
            dlg.Dispose();
            DisplayAlbum();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pbxPhoto_Click(object sender, EventArgs e)
        {

        }

        private void mnunew_Click(object sender, EventArgs e)
        {
            NewAlbum();
        }
        private void NewAlbum()
        {

            Manager = new AlbumManager();
            DisplayAlbum();
        }
        protected void menuFileOpen_Click(object sender, System.EventArgs e)
        {
            // Allow user to select a new album
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open Album";
            dlg.Filter = "Album files (*.abm)|*.abm|"
            + "All files (*.*)|*.*";
            dlg.InitialDirectory
            = AlbumManager.DefaultPath;
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string path = dlg.FileName;
                // TODO: save any existing album.
                try
                {
                    // Open the new album

                    Manager = new AlbumManager(path);
                }
                catch (AlbumStorageException aex)
                {
                    string msg = String.Format(
                        "Unable to open album file {0}\n({1})",
                        path, aex.Message);
                    MessageBox.Show(msg, "Unable to Open");
                    Manager = new AlbumManager();
                }
                DisplayAlbum();
            }
            dlg.Dispose();
        }
        private void SaveAlbum(string name)
        {
            try
            {
                Manager.Save(name, true);
            }
            catch (AlbumStorageException aex)
            {
                string msg = String.Format("Unable to save album {0} ({1})\n\n" + "Do you wish to save the album " + "under an alternate name?", name, aex.Message);
                DialogResult result = MessageBox.Show(msg, "Unable to Save", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                    SaveAsAlbum();
            }
        }
        private bool SaveAndCloseAlbum()
        {
            if (Manager.Album.HasChanged)
            {
                string msg;
                if (String.IsNullOrEmpty(Manager.FullName))
                    msg = "Do you wish to save your changes?";
                else
                    msg = String.Format("Do you wish to "

                        + "save your changes to \n{0}?",Manager.FullName);
                DialogResult result
                    = MessageBox.Show(this, msg,"Save Changes?",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                    SaveAlbum();
                else if (result == DialogResult.Cancel)
                    return false; // do not close
            }
            // Close the album and return true
            if (Manager.Album != null)
                Manager.Album.Dispose();
            Manager = new AlbumManager();
            SetTitleBar();
            return true;
// Offer to save the current album
        }
        private void SaveAlbum()
        {
             if (String.IsNullOrEmpty(Manager.FullName))
                 SaveAsAlbum();
             else
             {
                 // Save the album under the existing name
                 SaveAlbum(Manager.FullName);
             }
        }

        private void mnusave_Click(object sender, EventArgs e)
        {
            SaveAlbum();
        }

        protected void mnusaveas_Click(object sender, EventArgs e)
        {
            SaveAsAlbum();
        }
        private void SaveAsAlbum()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Title = "Save Album";
            dlg.DefaultExt = "abm";
            dlg.Filter = "Album files (*.abm)|*.abm|"
            + "All files|*.*";
            dlg.InitialDirectory
            = AlbumManager.DefaultPath;
            dlg.RestoreDirectory = true;
            dlg.Title = "Save Album";
            dlg.DefaultExt = "abm";
            dlg.Filter = "Album files (*.abm)|*.abm|"
            + "All files|*.*";
            dlg.InitialDirectory
            = AlbumManager.DefaultPath;
            dlg.RestoreDirectory = true;
            dlg.Dispose();
        }
        private void SetTitleBar()
        {
            Version ver = new Version(
            Application.ProductVersion);
            string name = Manager.FullName;
            this.Text = String.Format(
            "{2} - MyPhotos {0:0}.{1:0}",
            ver.Major, ver.Minor,
            String.IsNullOrEmpty(name)
            ? "Untitled" : name);
        }

        private void mnuremove_Click(object sender, EventArgs e)
        {
            if (Manager.Album.Count > 0)
            {
                Manager.Album.RemoveAt(Manager.Index);
                DisplayAlbum();
            }
        }
    }
}
