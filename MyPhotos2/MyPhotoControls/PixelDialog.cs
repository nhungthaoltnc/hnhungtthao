﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyPhotoControls
{
    public partial class PixelDialog : Form
    {
        public PixelDialog()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SetPixelData(int x, int y,int red, int green, int blue)
        {
            lbX.Text = x.ToString();
            lbY.Text = y.ToString();
            lbRed.Text = red.ToString();
            lbGreen.Text = green.ToString();
            lbBlue.Text = blue.ToString();
        }
    }
}
