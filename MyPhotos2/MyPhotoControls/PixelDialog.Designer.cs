﻿namespace MyPhotoControls
{
    partial class PixelDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbX = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbY = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbRed = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbGreen = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbBlue = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lbBlue, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbGreen, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbRed, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbY, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbX, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(142, 164);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(38, 171);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(67, 27);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbX
            // 
            this.lbX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbX.Location = new System.Drawing.Point(86, 9);
            this.lbX.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lbX.Name = "lbX";
            this.lbX.Size = new System.Drawing.Size(53, 25);
            this.lbX.TabIndex = 1;
            this.lbX.Text = "  ";
            this.lbX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 34);
            this.label3.TabIndex = 2;
            this.label3.Text = "Y";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbY
            // 
            this.lbY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbY.Location = new System.Drawing.Point(86, 43);
            this.lbY.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lbY.Name = "lbY";
            this.lbY.Size = new System.Drawing.Size(53, 25);
            this.lbY.TabIndex = 3;
            this.lbY.Text = "  ";
            this.lbY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 33);
            this.label5.TabIndex = 4;
            this.label5.Text = "Red";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbRed
            // 
            this.lbRed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbRed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbRed.Location = new System.Drawing.Point(86, 77);
            this.lbRed.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lbRed.Name = "lbRed";
            this.lbRed.Size = new System.Drawing.Size(53, 24);
            this.lbRed.TabIndex = 5;
            this.lbRed.Text = "  ";
            this.lbRed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 30);
            this.label7.TabIndex = 6;
            this.label7.Text = "Green";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbGreen
            // 
            this.lbGreen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGreen.Location = new System.Drawing.Point(86, 110);
            this.lbGreen.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lbGreen.Name = "lbGreen";
            this.lbGreen.Size = new System.Drawing.Size(53, 21);
            this.lbGreen.TabIndex = 7;
            this.lbGreen.Text = "  ";
            this.lbGreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 33);
            this.label9.TabIndex = 8;
            this.label9.Text = "Blue";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBlue
            // 
            this.lbBlue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbBlue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBlue.Location = new System.Drawing.Point(86, 140);
            this.lbBlue.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lbBlue.Name = "lbBlue";
            this.lbBlue.Size = new System.Drawing.Size(53, 24);
            this.lbBlue.TabIndex = 9;
            this.lbBlue.Text = "  ";
            this.lbBlue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PixelDialog
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(144, 202);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PixelDialog";
            this.Text = "Pixel Values";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lbBlue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbGreen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbRed;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbX;
        private System.Windows.Forms.Label label1;
    }
}

